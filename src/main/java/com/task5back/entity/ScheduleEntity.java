package com.task5back.entity;

import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "schedule", schema = "schoolmain")
public class ScheduleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "weekday", nullable = true, length = 10)
    private String weekday;
    @Column(name = "lessonsnumber", nullable = true)
    private Integer lessonsnumber;
}
