package com.task5back.entity;

import lombok.*;

import javax.persistence.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "peoplesprogress", schema = "schoolmain")
public class PeoplesprogressEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "averagescore", nullable = true)
    private Integer averagescore;
    @Column(name = "passesamount", nullable = true)
    private Integer passesamount;
}
