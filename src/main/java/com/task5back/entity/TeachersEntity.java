package com.task5back.entity;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "teachers", schema = "schoolmain")
//@JsonIgnoreType
public class TeachersEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "teacherfio", nullable = true, length = 40)
    private String teacherfio;
    /*@Column(name = "birhday", nullable = true)
    private Date birhday;*/
    @Column(name = "yearexperience", nullable = true)
    private Integer yearexperience;
    @Column(name = "leveleducation", nullable = true, length = 10)
    private String leveleducation;

    @JsonIgnore
    //@JsonRawValue

    @ManyToMany(cascade = {CascadeType.ALL, CascadeType.MERGE}, fetch = FetchType.EAGER)

    @JoinTable(name = "schoolgroups_teachers",
            joinColumns = @JoinColumn(name = "id_teachers"),
            inverseJoinColumns = @JoinColumn(name = "id_schoolgroups")
    )
    //@JsonBackReference
    //@JsonUnwrapped
    List<SchoolgroupsEntity> schoolgroups;

    /*@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeachersEntity that = (TeachersEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }*/
}
