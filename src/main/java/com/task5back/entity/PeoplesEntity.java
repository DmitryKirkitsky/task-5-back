package com.task5back.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "peoples", schema = "schoolmain")
public class PeoplesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "peoplesnfio", nullable = true, length = 40)
    private String peoplesnfio;
    @Column(name = "peoplesbirth", nullable = true)
    private Date peoplesbirth;
    @ManyToOne(cascade = {CascadeType.ALL, CascadeType.MERGE})
    @JoinColumn(name="id_schoolgroups", referencedColumnName = "id")
    private SchoolgroupsEntity idSchoolgroups;
}
