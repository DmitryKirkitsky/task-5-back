package com.task5back.interfaces;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("DAOInterface")
public interface DAOInterface<T> {
    void save(T t);
    void remove(T t);
    void update(T t);
    List<T> getAll();
    T getById(int id);
}
