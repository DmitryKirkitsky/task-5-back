package com.task5back.controllers;

import com.task5back.dao.SchoolgroupsDAO;
import com.task5back.dao.TeachersDAO;
import com.task5back.entity.SchoolgroupsEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/schoolgroups")

public class SchoolgroupsController {
    private final SchoolgroupsDAO schoolgroupsDAO;
    private final TeachersDAO teachersDAO;

    public SchoolgroupsController(SchoolgroupsDAO schoolgroupsDAO, TeachersDAO teachersDAO) {
        this.schoolgroupsDAO = schoolgroupsDAO;
        this.teachersDAO = teachersDAO;
    }


    @GetMapping
    public List<SchoolgroupsEntity> getSchoolgroups() {
        List<SchoolgroupsEntity> schoolgroupsEntities = schoolgroupsDAO.getAll();
        System.out.println(schoolgroupsEntities);
        return schoolgroupsEntities;
    }



}
