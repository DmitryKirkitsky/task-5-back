package com.task5back.controllers;

import com.task5back.dao.TeachersDAO;
import com.task5back.entity.SchoolgroupsEntity;
import com.task5back.entity.TeachersEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/teachers")
public class TeachersController {
    private final TeachersDAO teachersDAO;

    public TeachersController(TeachersDAO teachersDAO) {
        this.teachersDAO = teachersDAO;
    }


    @GetMapping
    public List<TeachersEntity> getTeachers() {
        List<TeachersEntity> teachersEntities = teachersDAO.getAll();
        System.out.println(teachersEntities);
        return teachersEntities;
    }
}
