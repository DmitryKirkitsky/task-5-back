package com.task5back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5BackApplication {

    public static void main(String[] args) {
        SpringApplication.run(Task5BackApplication.class, args);
    }

}
