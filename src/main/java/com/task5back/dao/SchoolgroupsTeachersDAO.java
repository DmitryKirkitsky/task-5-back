package com.task5back.dao;

import com.task5back.entity.SchoolgroupsTeachersEntity;
import com.task5back.interfaces.DAOInterface;
import com.task5back.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class SchoolgroupsTeachersDAO implements DAOInterface<SchoolgroupsTeachersEntity> {
    Session session;
    @Override
    public void save(SchoolgroupsTeachersEntity schoolgroupsTeachersEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(schoolgroupsTeachersEntity);
        transaction.commit();
    }

    @Override
    public void remove(SchoolgroupsTeachersEntity schoolgroupsTeachersEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(schoolgroupsTeachersEntity);
        transaction.commit();
    }

    @Override
    public void update(SchoolgroupsTeachersEntity schoolgroupsTeachersEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(schoolgroupsTeachersEntity);
        transaction.commit();
    }

    @Override
    public List<SchoolgroupsTeachersEntity> getAll() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<SchoolgroupsTeachersEntity> result = session.createQuery("from SchoolgroupsTeachersEntity").list();
        session.close();
        return result;
    }

    @Override
    public SchoolgroupsTeachersEntity getById(int id) {
        SchoolgroupsTeachersEntity schoolgroupsTeachersEntity;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        schoolgroupsTeachersEntity = session.get(SchoolgroupsTeachersEntity.class, id);
        session.close();
        return schoolgroupsTeachersEntity;
    }
}
