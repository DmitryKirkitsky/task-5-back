package com.task5back.dao;

import com.task5back.entity.PeoplesEntity;
import com.task5back.interfaces.DAOInterface;
import com.task5back.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PeoplesDAO implements DAOInterface<PeoplesEntity> {
    private Session session;
    @Override
    public void save(PeoplesEntity peoplesEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(peoplesEntity);
        transaction.commit();
    }

    @Override
    public void remove(PeoplesEntity peoplesEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(peoplesEntity);
        transaction.commit();
    }

    @Override
    public void update(PeoplesEntity peoplesEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(peoplesEntity);
        transaction.commit();
    }

    @Override
    public List<PeoplesEntity> getAll() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<PeoplesEntity> result = session.createQuery("from PeoplesEntity").list();
        session.close();
        return result;
    }

    @Override
    public PeoplesEntity getById(int id) {
        PeoplesEntity peoplesEntity;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        peoplesEntity = session.get(PeoplesEntity.class, id);
        session.close();
        return peoplesEntity;
    }
}
