package com.task5back.dao;

import com.task5back.entity.PeoplesprogressEntity;
import com.task5back.interfaces.DAOInterface;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.task5back.util.HibernateSessionFactoryUtil;

import java.util.List;

public class PeoplesprogressDAO implements DAOInterface<PeoplesprogressEntity> {
    private Session session;
    @Override
    public void save(PeoplesprogressEntity peoplesprogressEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(peoplesprogressEntity);
        transaction.commit();
    }

    @Override
    public void remove(PeoplesprogressEntity peoplesprogressEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(peoplesprogressEntity);
        transaction.commit();
    }

    @Override
    public void update(PeoplesprogressEntity peoplesprogressEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(peoplesprogressEntity);
        transaction.commit();
    }

    @Override
    public List<PeoplesprogressEntity> getAll() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<PeoplesprogressEntity> result = session.createQuery("from PeoplesprogressEntity").list();
        session.close();
        return result;
    }

    @Override
    public PeoplesprogressEntity getById(int id) {
        PeoplesprogressEntity peoplesprogressEntity;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        peoplesprogressEntity = session.get(PeoplesprogressEntity.class, id);
        session.close();
        return peoplesprogressEntity;
    }
}
