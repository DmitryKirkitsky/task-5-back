package com.task5back.dao;

import com.task5back.entity.LessonsEntity;
import com.task5back.interfaces.DAOInterface;
import com.task5back.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class LessonsDAO implements DAOInterface<LessonsEntity> {
    private Session session;

    @Override
    public void save(LessonsEntity lessonsEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(lessonsEntity);
        transaction.commit();
    }

    @Override
    public void remove(LessonsEntity lessonsEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(lessonsEntity);
        transaction.commit();
    }

    @Override
    public void update(LessonsEntity lessonsEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(lessonsEntity);
        transaction.commit();
    }

    @Override
    public List<LessonsEntity> getAll() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<LessonsEntity> result = session.createQuery("from LessonsEntity").list();
        session.close();
        return result;
    }

    @Override
    public LessonsEntity getById(int id) {
        LessonsEntity lessonsEntity;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        lessonsEntity = session.get(LessonsEntity.class, id);
        session.close();
        return lessonsEntity;
    }
}
