package com.task5back.dao;

import com.task5back.entity.ScheduleEntity;
import com.task5back.interfaces.DAOInterface;
import com.task5back.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ScheduleDAO implements DAOInterface<ScheduleEntity> {
    private Session session;
    @Override
    public void save(ScheduleEntity scheduleEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(scheduleEntity);
        transaction.commit();
    }

    @Override
    public void remove(ScheduleEntity scheduleEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(scheduleEntity);
        transaction.commit();
    }

    @Override
    public void update(ScheduleEntity scheduleEntity) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(scheduleEntity);
        transaction.commit();
    }

    @Override
    public List<ScheduleEntity> getAll() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<ScheduleEntity> result = session.createQuery("from ScheduleEntity").list();
        session.close();
        return result;
    }

    @Override
    public ScheduleEntity getById(int id) {
        ScheduleEntity scheduleEntity;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        scheduleEntity = session.get(ScheduleEntity.class, id);
        session.close();
        return scheduleEntity;
    }
}
