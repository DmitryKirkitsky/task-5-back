package com.task5back.dao;

import com.task5back.entity.TeachersEntity;
import com.task5back.interfaces.DAOInterface;
import com.task5back.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TeachersDAO implements DAOInterface<TeachersEntity> {

    @Override
    public void save(TeachersEntity teachersEntity) {
        Session session;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(teachersEntity);
        transaction.commit();
    }

    @Override
    public void remove(TeachersEntity teachersEntity) {
        Session session;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(teachersEntity);
        transaction.commit();
    }

    @Override
    public void update(TeachersEntity teachersEntity) {
        Session session;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(teachersEntity);
        transaction.commit();
    }

    @Override
    public List<TeachersEntity> getAll() {
        Session session;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<TeachersEntity> result = session.createQuery("from TeachersEntity").list();
        session.close();
        return result;
    }

    @Override
    public TeachersEntity getById(int id) {
        Session session;
        TeachersEntity teachersEntity;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        teachersEntity = session.get(TeachersEntity.class, id);
        session.close();
        return teachersEntity;
    }
}
