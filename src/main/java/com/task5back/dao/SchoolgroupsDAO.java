package com.task5back.dao;

import com.task5back.entity.SchoolgroupsEntity;
import com.task5back.interfaces.DAOInterface;
import com.task5back.util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SchoolgroupsDAO implements DAOInterface<SchoolgroupsEntity> {

    @Override
    public void save(SchoolgroupsEntity schoolgroupsEntity) {
        Session session;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(schoolgroupsEntity);
        transaction.commit();
    }

    @Override
    public void remove(SchoolgroupsEntity schoolgroupsEntity) {
        Session session;
        setPersontWithSuchFacultyToNull(schoolgroupsEntity);
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(schoolgroupsEntity);
        transaction.commit();
    }

    @Override
    public void update(SchoolgroupsEntity schoolgroupsEntity) {
        Session session;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(schoolgroupsEntity);
        transaction.commit();
    }


    @Override
    public List<SchoolgroupsEntity> getAll() {
        Session session;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<SchoolgroupsEntity> result = session.createQuery("from SchoolgroupsEntity").list();
        session.close();
        return result;
    }

    @Override
    public SchoolgroupsEntity getById(int id) {
        Session session;
        SchoolgroupsEntity schoolgroupsEntity;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        schoolgroupsEntity = session.get(SchoolgroupsEntity.class, id);
        session.close();
        return schoolgroupsEntity;
    }
    public void setPersontWithSuchFacultyToNull(SchoolgroupsEntity schoolgroupsEntity) {
        Session session;
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        schoolgroupsEntity.setTeachers(null);
        session.merge(schoolgroupsEntity);
        transaction.commit();
        session.close();
    }



}
