package com.task5back.util;

import com.task5back.entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.logging.Level;
import java.util.logging.Logger;

public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;
    static Logger logger;
    private HibernateSessionFactoryUtil() {}

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(LessonsEntity.class);
                configuration.addAnnotatedClass(PeoplesEntity.class);
                configuration.addAnnotatedClass(TeachersEntity.class);
                configuration.addAnnotatedClass(ScheduleEntity.class);
                configuration.addAnnotatedClass(SchoolgroupsEntity.class);
                configuration.addAnnotatedClass(PeoplesprogressEntity.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());
            } catch (Exception e) {
                logger.log(Level.INFO,"Exception: ", e);
            }
        }
        return sessionFactory;
    }
}
